# Decision Science Hub Workshop: A hands-on introduction to Fourier analysis for understanding and controlling visual image properties in psychology and neuroscience experiments

This repository contains code for the Fourier analysis workshop, presented by Dr. Hinze Hogendoorn.

There are two directories: 'matlabpyrtools' and 'Hinze Fourier Demo HUB'. The first directory contains a toolbox of functions that we will use in the workshop, and should be added to the MATLAB path. 
The second directory contains extra code that Hinze has prepared for the workshop.

Workshop presented on the 20th September, 2018, as part of the Decision Science Hub workshop series.

