function [qDown]=HH_checkQuit
%
% Checks for the Q key to be down. Use to abort while loops during
% debugging. ie (if HH_checkQuit 
%                   break
%                end)
%
qDown=0;
[keyIsDown,secs,keyCode] = KbCheck;
if strcmp(KbName(keyCode),'q'),
    qDown=1;
end