% MakeMesh
function [X,Y,d,a]=MakeMesh(m,n,offX,offY)
if ~exist('n')
    n=m;
end
if ~exist('offX')
    offX=0;
end
if ~exist('offY')
    offY=0;
end
[X,Y]=meshgrid([-n/2:n/2-1]-offY,[-m/2:m/2-1]-offX);
d=sqrt(X.^2+Y.^2);
t1=(d==0);
a=atan2(Y,X);