% MakeAngMap
function aMap=MakeAngMap(m,n,ThetaPeak,ThetaSigma)
[X Y d a]=MakeMesh(m,n);
s1=sin(a); c1=cos(a);
    	  	ds = s1 * cos(ThetaPeak) - c1 * sin(ThetaPeak);     
		  	dc = c1 * cos(ThetaPeak) + s1 * sin(ThetaPeak);
		  	ds2 = s1 * cos(pi+ThetaPeak) - c1 * sin(pi+ThetaPeak);     
		  	dc2 = c1 * cos(pi+ThetaPeak) + s1 * sin(pi+ThetaPeak);
	  		dtheta  = abs(atan2(ds,dc)); % Absolute angular distance.
	  		dtheta2 = abs(atan2(ds2,dc2)); 
	  		aMap = exp((-dtheta.^2) /(2*ThetaSigma^2))+exp((-dtheta2.^2)/(2*ThetaSigma^2));% Angular filter component.
