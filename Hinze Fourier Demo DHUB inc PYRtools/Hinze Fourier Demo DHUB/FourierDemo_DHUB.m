% Fourier Demo
%
%

%%%%%%
% 1D %
%%%%%%

stimSize=256;

period1=256;% frequency is 1/256 (cycles/pixel)
period2=64; % frequency is 1/64  (cycles/pixel)
period3=16; % frequency is 1/16  (cycles/pixel)
period4=4;  % frequency is 1/4   (cycles/pixel)

signal1=sin( 2*pi/period1 * [1:stimSize]);
signal2=sin( 2*pi/period2 * [1:stimSize]);
signal3=sin( 2*pi/period3 * [1:stimSize]);
signal4=sin( 2*pi/period4 * [1:stimSize]);

subplot(4,2,1);     plot(1:stimSize,signal1);    axis([1 stimSize -2 2]);
subplot(4,2,3);     plot(1:stimSize,signal2);    axis([1 stimSize -2 2]);
subplot(4,2,5);     plot(1:stimSize,signal3);    axis([1 stimSize -2 2]);
subplot(4,2,7);     plot(1:stimSize,signal4);    axis([1 stimSize -2 2]);


%FFT!

fft(signal1)

subplot(4,2,2);
plot(abs(fft(signal1)));    axis([-10 267 -10 150]);

subplot(4,2,4);
plot(abs(fft(signal2)));    axis([-10 267 -10 150]);

subplot(4,2,6);
plot(abs(fft(signal3)));    axis([-10 267 -10 150]);

subplot(4,2,8);
plot(abs(fft(signal4)));    axis([-10 267 -10 150]);

% what does the axis mean?
%
% Axis goes from LOWEST frequency in the data (DC) to HIGHEST DETECTABLE frequency
% (sampling rate/2 - the Nyquist frequency) and back again


subplot(4,2,2);
plot(fftshift(abs(fft(signal1))));    axis([-10 267 -10 150]);

subplot(4,2,4);
plot(fftshift(abs(fft(signal2))));    axis([-10 267 -10 150]);

subplot(4,2,6);
plot(fftshift(abs(fft(signal3))));    axis([-10 267 -10 150]);

subplot(4,2,8);
plot(fftshift(abs(fft(signal4))));    axis([-10 267 -10 150]);

% What does the axis mean?
% it goes from negative nyquist to positive nyquist freq!
%
% YOU ALWAYS NEED AT LEAST 2 POINTS (IF YOU'RE LUCKY!!!) TO DEFINE A WAVE!
%
% so it goes from -0.5 to 0.5

subplot(4,2,2);
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal1))));    axis([-0.6 0.6 -10 150]);

subplot(4,2,4);
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal2))));    axis([-0.6 0.6 -10 150]);

subplot(4,2,6);
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal3))));    axis([-0.6 0.6 -10 150]);

subplot(4,2,8);
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal4))));    axis([-0.6 0.6 -10 150]);

for i=1:4
    subplot(4,2,i*2);xlabel('cycles per pixel')
end

% show dc offset

signal5=signal3+0.3;

subplot(4,2,5);hold on;
plot(1:stimSize,signal5,'r');
hold off

subplot(4,2,6);
hold on;
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal5))),'r');
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal3))));
hold off;


%WHAT ABOUT THE PHASE?!

subplot(4,2,1);
signal1=sin( 3 +  2*pi/period1 * [1:stimSize]);  % changing phase around does NOT affect frequency spectrum!
plot(1:stimSize,signal1); axis([1 stimSize -2 2]);

subplot(4,2,2);
plot([-0.5:1/stimSize:0.5-1/stimSize],fftshift(abs(fft(signal1))));   axis([-0.6 0.6 -10 150]); xlabel('cycles per pixel')

return



% finally: Square waves!

figure
signal2=sin( 2*pi/64 * [1:stimSize]);

subplot(2,2,1); plot(signal2); axis([0 257 -2 2]);
subplot(2,2,2); plot(fftshift(abs(fft(signal2))));

subplot(2,2,3); plot(2*round(signal2*0.5+0.5)-1);axis([0 257 -2 2]);
subplot(2,2,4); plot(fftshift(abs(fft(2*round(signal2*0.5+0.5)-1))));

figure;
subplot(2,2,1);plot(2*round(signal2*0.5+0.5)-1);axis([0 257 -2 2]);

signal2b=sin( 2*pi/64 * [1:stimSize]);
signal2c=sin( 2*pi/(64/3) * [1:stimSize]);
signal2d=sin( 2*pi/(64/5) * [1:stimSize]);

subplot(2,2,2);plot(signal2b);axis([0 257 -2 2]);
subplot(2,2,3);plot(signal2c/2);axis([0 257 -2 2]);
subplot(2,2,4);plot(signal2d/3);axis([0 257 -2 2]);
subplot(2,2,1);hold on;

plot(signal2b+signal2c/2+signal2d/3,'r')%+signal3/2+signal4/3,'r');

% THIS IS ALL VERY SHORT, BUT LET'S GET TO PICTURES!


%%%%%%
% 2D %
%%%%%%

figure



stimSize=[128 128];



%% sine waves:
imSine1V=mkSine(stimSize,16,0,1,0);
imSine2V=mkSine(stimSize,64,0,1,0);
imSine1H=mkSine(stimSize,16,pi/2,1,0);
imSine2H=mkSine(stimSize,64,pi/2,1,0);

%% square waves:
imSine1V=2*round(0.5+0.5*mkSine(stimSize,16,0,1,0))-1;
imSine2V=2*round(0.5+0.5*mkSine(stimSize,64,0,1,0))-1;
imSine1H=2*round(0.5+0.5*mkSine(stimSize,16,pi/2,1,0))-1;
imSine2H=2*round(0.5+0.5*mkSine(stimSize,64,pi/2,1,0))-1;

%% oblique sine waves:
imSine1V=mkSine(stimSize,16,0,1,0);
imSine2V=mkSine(stimSize,64,-pi/4,1,0);
imSine1H=mkSine(stimSize,16,pi/4,1,0);
imSine2H=mkSine(stimSize,64,-pi/4,1,0);


subplot(1,2,1); plot(imSine1V(1,:),'b');
hold on; plot(imSine2V(1,:),'r'); axis([1 128 -2 2]); hold off;
subplot(1,2,2); plot(fftshift(abs(fft(imSine1V(1,:)))),'b');
hold on; plot(fftshift(abs(fft(imSine2V(1,:)))),'r'); axis([1 128 -2 100]);hold off;

figure;
showIm([    imSine1V imSine2V ;
    imSine1H imSine2H] );



imSine1V_fft=fft2(imSine1V);
imSine2V_fft=fft2(imSine2V);
imSine1H_fft=fft2(imSine1H);
imSine2H_fft=fft2(imSine2H);

 figure

showIm( [   fftshift(abs(imSine1V_fft)) fftshift(abs(imSine2V_fft))  ;
    fftshift(abs(imSine1H_fft)) fftshift(abs(imSine2H_fft)) ],[0 2000]);

return

%%%%%%%%%%%%%%%%%%
% Natural Images %
%%%%%%%%%%%%%%%%%%

figure;
im=double(imread('sinatra.tif','TIFF'));
subplot(2,3,1);showIm(im);

imSpectrum=abs(fft2(im));
subplot(2,3,4); showIm(fftshift(imSpectrum),[0 10000]);


imLoPass=filter2(mkGaussian(30),im,'full'); %% Also show 'full' !
imLoPassSpectrum=abs(fft2(imLoPass));
subplot(2,3,2); showIm(imLoPass);
subplot(2,3,5); showIm(fftshift(imLoPassSpectrum),[0 10000]);

imHiPass=im-filter2(mkGaussian(9),im,'same');
imHiPassSpectrum=abs(fft2(imHiPass));
subplot(2,3,3); showIm(imHiPass);
subplot(2,3,6); showIm(fftshift(imHiPassSpectrum),[0 10000]);


return

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reconstructing an image %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
im=double(imread('pitt.tif','TIFF'));
subplot(1,3,1);showIm(im); title('original')

fft2(im)

im_fft=fft2(im);

% get the frequencies:
imSpectrum=abs(im_fft);
subplot(2,3,2); showIm(fftshift(imSpectrum),[0 10000]); title('power spectrum')

% get the phases:
imPhase=angle(im_fft);
subplot(2,3,5); showIm(imPhase); title('phase');

% reconstruct
% IFFT function requires matrix of complex numbers - so put the matrix of
% amplitudes and angles together
imReconstructed=ifft2(imSpectrum.*exp(sqrt(-1)*imPhase));

% for some reason, you're left with complex junk... just take abs again
imReconstructed=abs(imReconstructed);

subplot(1,3,3); showIm(imReconstructed); title('reconstructed')

return

%%%%%%%%%%%%%%%%%%%%
% Phase Scrambling %
%%%%%%%%%%%%%%%%%%%%

imPhase
phaseNoise=rand(size(imPhase))*2*pi-pi
noiseLevel=0.9;

imNoisyPhase= (1-noiseLevel) * imPhase + noiseLevel * phaseNoise;

subplot(2,3,5);showIm(imNoisyPhase); title('noisy phase');

imReconstructed=ifft2(imSpectrum.*exp(sqrt(-1)*imNoisyPhase));
subplot(1,3,3); showIm(abs(imReconstructed),[0 255]); title('reconstructed')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demo of Appearance from noise: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;

im=mean(double(imread('wieisdit.jpg','JPEG')),3);
im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);
phaseNoise=rand(size(imPhase))*2*pi-pi;

for noiseLevel=[1:-0.01:0]
    
    imNoisyPhase= (1-noiseLevel) * imPhase + noiseLevel * phaseNoise;
    imReconstructed=abs(ifft2(imSpectrum.*exp(sqrt(-1)*imNoisyPhase)));
    showIm(imReconstructed,[0 255]);
    drawnow
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Filtering (playing with the spectrum) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure

im=mean(double(imread('house5.jpg','JPEG')),3);
% im=double(imread('sinatra.tif','TIFF'));
im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);

subplot(2,3,1);showIm(im,[0 255]); title('original')
subplot(2,3,2);showIm(fftshift(imSpectrum),[0 10000]); title('spectrum');

maskAngle=pi/2;
maskWidth=pi/10;

spectrumMask=MakeAngMap(size(im,1),size(im,2),maskAngle,maskWidth);    % Make a pretty bowtie
subplot(2,3,3);showIm(spectrumMask); title('spectrum mask');

newSpectrum=imSpectrum .* fftshift(spectrumMask);
subplot(2,3,4);showIm(fftshift(newSpectrum),[0 10000]); title('new spectrum');

imReconstructed=ifft2(newSpectrum.*exp(sqrt(-1)*imPhase));

subplot(2,3,6); showIm(abs(imReconstructed)); title('Reconstructed image')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DEMO of face appearing from bar code %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure

im=mean(double(imread('wieisdit2.jpg','JPEG')),3);
im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);


for maskWidth=pi/100:pi/1000:pi/2
    
    spectrumMask=Trunc(MakeAngMap(size(im,1),size(im,2),pi,maskWidth));    % Make a pretty bowtie
    
    newSpectrum=imSpectrum .* fftshift(spectrumMask);
    imReconstructed=abs(ifft2(newSpectrum.*exp(sqrt(-1)*imPhase)));
    
    subplot(1,2,1)
    showIm(abs(fftshift(newSpectrum)),[0 10000]);
    subplot(1,2,2)
    showIm(imReconstructed);
    drawnow
end

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% What about Hi-pass and Lo-pass? %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The filter2() function is your friend! (and so's mkGaussian)

help filter2

figure

im=mean(double(imread('house12.jpg','JPEG')),3);

subplot(2,3,1);showIm(im);

% lopass:
myFilter=mkGaussian(21);
subplot(2,3,2);showIm(myFilter,[-0.02 0.02]);

imFiltered=filter2(myFilter,im);
subplot(2,3,3);showIm(imFiltered);

help mkGaussian

%hipass:
myFilter= - mkGaussian(21); myFilter(11,11)=myFilter(11,11)+1;
subplot(2,3,2);showIm(myFilter,[-0.02 0.02]);

imFiltered=filter2(myFilter,im);
subplot(2,3,3);showIm(imFiltered);

% But it's easier! Hipass is simply subtracting the Lopass picture from the
% original:

myFilter=mkGaussian(21); % <--- lopass!
imFiltered=im-filter2(myFilter,im);
subplot(2,3,6); showIm(imFiltered)

% And what about bandpass?
% eliminate both high frequencies and low frequencies. How do you do that?
% Subtract two blurred images from each other!

myFilterBroad=mkGaussian(51);
myFilterNarrow=mkGaussian(21);

imFilter1=filter2(myFilterBroad,im);
subplot(2,3,4); showIm(imFilter1); title('LoPass Low')

imFilter2=filter2(myFilterNarrow,im);
subplot(2,3,5); showIm(imFilter2); title('LoPass High')

subplot(2,3,6); showIm(imFilter2-imFilter1); title('BandPass')

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% What's so good about Gabors? %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure

im=mkSine([256 256],32,0,1,0);
im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);

subplot(2,4,1); showIm(im)
subplot(2,4,5); showIm(fftshift(imSpectrum),[0 100])

% looks fine! BUT don't forget the window has hard edges!

im=zeros(256); im(65:192,65:192)=mkSine([128 128],32,0,1,0);
im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);
subplot(2,4,2); showIm(im)
subplot(2,4,6); showIm(fftshift(imSpectrum),[0 100])

% trick: make a window (for example, a gaussian window)

window=mkDisc(256);
window=mkDisc(256,64,[128.5 128.5],20);
window=mkDisc(256,64,[128.5 128.5],40);
window=mkDisc(256,64,[128.5 128.5],80);
window=mkGaussian(256);window =window/max(window(:));

subplot(2,4,3);showIm(window);

im=mkSine([256 256],32,0,1,0);
im=im.*window;
subplot(2,4,4); showIm(im)

im_fft=fft2(im);
imSpectrum=abs(im_fft);
imPhase=angle(im_fft);
subplot(2,4,8); showIm(fftshift(imSpectrum),[0 100])


return

%%%%%%%%%%%%%%%%%%%%%%%
% Directional Filters %
%%%%%%%%%%%%%%%%%%%%%%%

figure

im=mean(double(imread('house12.jpg','JPEG')),3);
subplot(2,3,1); showIm(im);

myFilter=mkSine([9 9],2*pi,pi/2,1,0);
filterMask=mkGaussian(9) / sum(sum(mkGaussian(9)));
% filterMask=mkDisc(9) / sum(sum(mkDisc(9)));

subplot(2,3,2);showIm(myFilter.*filterMask);

subplot(2,3,3);showIm(filter2(myFilter.*filterMask,im));

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Motion without movement %
%%%%%%%%%%%%%%%%%%%%%%%%%%%


th=0;
while ~HH_checkQuit
    th=th+pi/10;
    myFilter=mkSine([9 9],4,pi/2,1,th);

    subplot(2,3,2);showIm(flipud(myFilter.*filterMask),[-0.1 0.1]);

    subplot(2,3,3);showIm(filter2(myFilter.*filterMask,im),[-70 70]);
    drawnow
end

figure;
th=0:pi/10:2*pi-pi/10;

for fnum=1:20
    filterStack(:,:,fnum)=mkSine([9 9],4,pi/2,1,th(fnum));
    imStack(:,:,fnum)=filter2(filterStack(:,:,fnum).*filterMask,im);
    filterStack(:,:,fnum)=flipud(filterStack(:,:,fnum)).*filterMask;
end

%put little filter in upper left corner
imStack(1:9,1:9,:)=500*(filterStack);

fnum=1;
while ~HH_checkQuit
    fnum=mod(fnum,20)+1;

    showIm(imStack(:,:,fnum),[-50 50],2);
    drawnow;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILTERS AS A SEARCH MECHANISM %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure;
im=mean(double(imread('IMG0023.jpg','jpeg')),3);
tankje=mean(double(imread('tankje.tif','TIFF')),3);
showIm(im,[0 255],0.5);

subplot(2,2,1)
showIm(im,'auto',0.5)
subplot(2,2,2)
showIm(tankje,'auto',2)

tankje=tankje-mean(tankje(:));
tankje2=tankje.*mkGaussian(100);
% tankje2=tankje2-mean(tankje2(:));
subplot(2,2,4)
showIm(tankje2,'auto',2)

imNew=filter2(tankje2,im);
subplot(2,2,3)
showIm(imNew,'auto',0.5)

figure

im2=im-mean(im(:)); im2=im2.*(imNew).^3;

showIm(im);
showIm((imNew).^1)
showIm(im2);
